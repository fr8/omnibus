= Omnibus
:url-project: https://freighttrust.com
:url-docs: https://ft-docs.netlify.app
:url-org: https://gitlab.com/fr8
:url-repo: https://github.com/freight-trust
:url-group: {url-org}/omnibus
:url-site-readme: {url-group}/README.adoc
:url-opendevise: https://freighttrust.com

Primary documentation components are located in the {url-site-readme}[Omnibus].

== Resources

* {url-project}[Freight Trust & Clearing Corporation]
* {url-org}[Corporate Repositories]
* {url-org}[Corporate Repositories]



=== Copyright and License

Copyright (C) 2020 FreightTrust and Clearing Corporation

Use of this software is granted under the terms of the https://www.mozilla.org/en-US/MPL/2.0/[Mozilla Public License Version 2.0] (MPL-2.0).
See link:LICENSE[] to find the full license text.

==== Imprint

Development of the Freight Trust Network is led and sponsored by [FreightTrust & Clearing, Inc].